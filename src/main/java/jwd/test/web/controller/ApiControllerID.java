package jwd.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.test.model.Zadatak;
import jwd.test.service.ZadatakService;
import jwd.test.support.ZadatakToZadatakDTO;
import jwd.test.web.dto.ZadatakDTO;


@RestController
@RequestMapping(value = "/api/projekti/{id}/zadaci")
public class ApiControllerID {

	@Autowired
	ZadatakService service;
	@Autowired
	ZadatakToZadatakDTO toDto;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ZadatakDTO>> get(@PathVariable Long id, @RequestParam(defaultValue="0")int pageNum){
		
		Page<Zadatak> page = service.findById(id, pageNum);
		List<Zadatak> retVal = page.getContent();
		if(retVal == null || retVal.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<ZadatakDTO>>( toDto.convert(retVal) ,HttpStatus.OK);
	}
}
