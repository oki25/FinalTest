package jwd.test.web.dto;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import jwd.test.model.Projekat;
import jwd.test.model.Stanje;

public class ZadatakDTO {
	
	private Long id;
	
	@NotEmpty
	@Size(max=40)
	private String naziv;
	
	@NotEmpty
	private String zaduzeni;
	
	@Min(1)
	private Integer sati;
	
	private String opis;
	
	
	private String projekatNaziv;
	
	private Long projekatId;
	
	private String stanjeNaziv;
	
	private Long stanjeId;

	public ZadatakDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getZaduzeni() {
		return zaduzeni;
	}

	public void setZaduzeni(String zaduzeni) {
		this.zaduzeni = zaduzeni;
	}

	public Integer getSati() {
		return sati;
	}

	public void setSati(Integer sati) {
		this.sati = sati;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getProjekatNaziv() {
		return projekatNaziv;
	}

	public void setProjekatNaziv(String projekatNaziv) {
		this.projekatNaziv = projekatNaziv;
	}

	public Long getProjekatId() {
		return projekatId;
	}

	public void setProjekatId(Long projekatId) {
		this.projekatId = projekatId;
	}

	public String getStanjeNaziv() {
		return stanjeNaziv;
	}

	public void setStanjeNaziv(String stanjeNaziv) {
		this.stanjeNaziv = stanjeNaziv;
	}

	public Long getStanjeId() {
		return stanjeId;
	}

	public void setStanjeId(Long stanjeId) {
		this.stanjeId = stanjeId;
	}
	
	
	
}
