package jwd.test.service;



import java.util.List;

import org.springframework.data.domain.Page;


import jwd.test.model.Zadatak;

public interface ZadatakService {
	Page<Zadatak> findAll(int pageNum);
	Zadatak findOne(Long id);
	void save(Zadatak stand);
	void remove(Long id);
	Page<Zadatak> findById(Long id, int pageNum);
	Integer totalHours(List<Zadatak> list);
	
	void promeniStanje(Long id);
	
	Page<Zadatak> pretraga(
			String naziv, 
			String zaduzeni, 
			Long projekatId,
			int pageNum);

	
	
}
