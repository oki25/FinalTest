package jwd.test;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import jwd.test.model.Projekat;
import jwd.test.model.Stanje;
import jwd.test.model.Zadatak;

import jwd.test.service.ProjekatService;
import jwd.test.service.StanjeService;
import jwd.test.service.ZadatakService;

@Component
public class TestData {

	
	
	@Autowired
	private ProjekatService projekatService;
	
	@Autowired
	private ZadatakService zadatakService;
	
	@Autowired
	private StanjeService stanjeService;

	@PostConstruct
	public void init(){
	     
		Projekat p1 = new Projekat("Back-end", "21.05.2018.");	
		Projekat p2 = new Projekat("Front-end", "28.08.2018.");	 
	    
		
		
		Stanje s1 = new Stanje("Started");
		Stanje s2 = new Stanje("Working");
		Stanje s3 = new Stanje("Finished");
		
		
		
		
		Zadatak z1 = new Zadatak("Modelovanje baze", "Igor Ivanic", 10, "Modelovanje", p1, s1);
		Zadatak z2 = new Zadatak("Modelovanje entiteta", "Marko Markovic", 7, "Modelovanje", p1, s1);
		Zadatak z3 = new Zadatak("Upit", "Pera Peric", 1, "Modelovanje", p1, s2);
		Zadatak z4 = new Zadatak("CSS", "Jovan Jovic", 11, "Modelovanje", p2, s2);
		Zadatak z5 = new Zadatak("HTML", "Mika Miric", 11, "Modelovanje", p2, s2);
		Zadatak z6 = new Zadatak("Angular", "Stafan Stevic", 11, "Modelovanje", p2, s2);
		
		p1.addZadatak(z1);
		p1.addZadatak(z2);
		p1.addZadatak(z3);
		p2.addZadatak(z4);
		p2.addZadatak(z5);
		p2.addZadatak(z6);
		
		s1.addZadatak(z1);
		s1.addZadatak(z2);
		s2.addZadatak(z3);
		s2.addZadatak(z4);
		s2.addZadatak(z5);
		s2.addZadatak(z6);
		
		
		
		
		
		
		
		projekatService.save(p1);
		projekatService.save(p2);
		
		stanjeService.save(s1);
		stanjeService.save(s2);
		stanjeService.save(s3);
		
		
		zadatakService.save(z1);
		zadatakService.save(z2);
		zadatakService.save(z3);
		zadatakService.save(z4);
		zadatakService.save(z5);
		zadatakService.save(z6);
	}
}
