package jwd.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.test.model.Zadatak;
import jwd.test.service.ZadatakService;
import jwd.test.support.ZadatakDTOtoZadatak;
import jwd.test.support.ZadatakToZadatakDTO;
import jwd.test.web.dto.ZadatakDTO;

@RestController
@RequestMapping("/api/zadaci")
public class ApiZadatakController {
	@Autowired
	private ZadatakService service;
	@Autowired
	private ZadatakToZadatakDTO toDTO;
	@Autowired
	private ZadatakDTOtoZadatak toZadatak;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ZadatakDTO>> get(@RequestParam(required = false) String naziv,
			@RequestParam(required = false) String zaduzeni, @RequestParam(required = false) Long projekatId,
			@RequestParam(defaultValue = "0") int pageNum) {

		Page<Zadatak> page;
		if (naziv != null || zaduzeni != null || projekatId != null) {

			page = service.pretraga(naziv, zaduzeni, projekatId, pageNum);
		} else {
			page = service.findAll(pageNum);
		}

		List<Zadatak> retVal = page.getContent();

		if (retVal == null || retVal.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(page.getTotalPages()));

		headers.add("ukupnoSati", Integer.toString(service.totalHours(retVal)));

		return new ResponseEntity<>(toDTO.convert(retVal), headers, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<ZadatakDTO> get(@PathVariable Long id) {
		Zadatak retVal = service.findOne(id);

		if (retVal == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<ZadatakDTO>(toDTO.convert(retVal), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<ZadatakDTO> add(@Validated @RequestBody ZadatakDTO created) {

		Zadatak converted = toZadatak.convert(created);
		service.save(converted);

		return new ResponseEntity<>(toDTO.convert(converted), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/promena")
	public ResponseEntity<ZadatakDTO> promena(@PathVariable Long id) {

		Zadatak zadatak = service.findOne(id);
		if (zadatak == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if (zadatak.getStanje().getNaziv().equals("Finished")) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		service.promeniStanje(id);

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public ResponseEntity<ZadatakDTO> edit(@PathVariable Long id, @Validated @RequestBody ZadatakDTO edited) {

		if (!id.equals(edited.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Zadatak converted = toZadatak.convert(edited);
		service.save(converted);

		return new ResponseEntity<>(toDTO.convert(converted), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<ZadatakDTO> delete(@PathVariable Long id) {

		Zadatak delete = service.findOne(id);

		if (delete == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			service.remove(id);
		}

		return new ResponseEntity<ZadatakDTO>(toDTO.convert(delete), HttpStatus.OK);
	}

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
