package jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.test.model.Zadatak;
import jwd.test.web.dto.ZadatakDTO;

@Component
public class ZadatakToZadatakDTO 
	implements Converter<Zadatak, ZadatakDTO> {

	@Override
	public ZadatakDTO convert(Zadatak src) {
		ZadatakDTO dto = new ZadatakDTO();
		dto.setId(src.getId());
		dto.setNaziv(src.getNaziv());
		dto.setOpis(src.getOpis());
		dto.setSati(src.getSati());
		dto.setZaduzeni(src.getZaduzeni());
		
		if(src.getProjekat() != null) {
			dto.setProjekatId(src.getProjekat().getId());
			dto.setProjekatNaziv(src.getProjekat().getNaziv());
		}
		
		if(src.getStanje() != null) {
			dto.setStanjeId(src.getStanje().getId());
			dto.setStanjeNaziv(src.getStanje().getNaziv());
		}
		
		
		return dto;
	}
	
	public List<ZadatakDTO> convert(List<Zadatak> src){
		List<ZadatakDTO> dtos = new ArrayList<>();
		
		for(Zadatak x : src){
			dtos.add(convert(x));
		}
		
		return dtos;
	}

}
