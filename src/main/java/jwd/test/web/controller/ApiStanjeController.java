package jwd.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.test.model.Stanje;
import jwd.test.service.StanjeService;
import jwd.test.support.StanjeToStanjeDTO;
import jwd.test.web.dto.StanjeDTO;

@RestController
@RequestMapping(value = "/api/stanja")
public class ApiStanjeController {

	@Autowired
	StanjeService service;

	@Autowired
	StanjeToStanjeDTO toDto;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<StanjeDTO>> getAll(@RequestParam(defaultValue = "0") int pageNum) {

		Page<Stanje> page = service.findAll(pageNum);

		List<Stanje> retVal = page.getContent();
		if (retVal == null || retVal.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<StanjeDTO>>(toDto.convert(retVal), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<StanjeDTO> getOne(@PathVariable Long id) {

		Stanje retVal = service.findOne(id);

		if (retVal == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<StanjeDTO>(toDto.convert(retVal), HttpStatus.OK);
	}
}
