var app = angular.module("test", ['ngRoute']);

app.controller("ctrl", function($scope){
	
	$scope.message = "Test JWD28!";
	
});

//MAIN kontroler
app.controller("mainCtrl", function($scope, $http, $location){
	
	//********************************
	//dobavljanje svih automobila
	var baseUrl = "/api/zadaci";
	$scope.zadaci = [];
	
	//********************************
	//pretraga
	$scope.search = {};
	$scope.search.naziv = "";
	$scope.search.zaduzeni = "";
	$scope.search.projekatId = "";
	
	$scope.ponisti = function(){
		$scope.search.naziv = "";
		$scope.search.zaduzeni = "";
		$scope.search.projekatId = "";
		$scope.pageNum = 0;
		getAll();
	}


	
	$scope.pretraga = function(){
		$scope.pageNum = 0;
		getAll();
	}
	
	var getAll = function(){
		
		
		
		var config = { params: {} };
		
		if($scope.search.naziv != ""){
			config.params.naziv = $scope.search.naziv;
		}
		if($scope.search.zaduzeni != ""){
			config.params.zaduzeni = $scope.search.zaduzeni;
		}
		if($scope.search.projekatId != ""){
			config.params.projekatId = $scope.search.projekatId;
		}
		
		config.params.pageNum = $scope.pageNum;
		
		var promise = $http.get(baseUrl, config)
		
		
		promise.then(
			
			function success(res){
				
				$scope.zadaci = res.data;
				$scope.totalPages = res.headers("totalPages");
				$scope.ukupnoSati = res.headers("ukupnoSati")
				
			},
			function error(res){
				alert("Ooops! An error has ocurred, plese try again!");
			}
		);
	}
	getAll();

	
	$scope.ukupnoSati = "";
	
	
	//********************************
	//brisanje
	$scope.delete=function(id){
			
		$http.delete(baseUrl +"/"+ id).then(
				function success(res){
					$scope.pageNum=0;
					getAll();
				},
				function error(res){
					alert("Ooops! An error has ocurred, plese try again!");
				}
			);
	}
	//********************************
	var baseUrlProjekti = "/api/projekti/"
	$scope.projekti = [];

	var getProjekti = function(){
		var promise = $http.get(baseUrlProjekti);
		promise.then(
			function success(res){
				$scope.projekti = res.data;
				
			},
			function error(res){
				alert("Ooops! An error has ocurred, plese try again!")
			}
		)
	}
	getProjekti();


	$scope.promeniStanje = function(id){
		var promise = $http.post(baseUrl + "/" + id + "/promena");
		promise.then(
			function success(res){
				alert("Stanje je promenjeno!")
				getAll();
			},
			function error(res){
				alert("Promena stanja nije uspela!")
			}
		)
	}

	//********************************
	//dodavanje novog
	$scope.dodaj = function(){
		$location.path("/add")
	}
	
	//********************************
	//izmena postojeceg
	$scope.izmeni = function(id){
		$location.path("/edit/" + id)
	}
	
	//********************************
	//paginacija
	$scope.pageNum = 0;
	$scope.totalPages = 1;
	$scope.go = function(direction){
		$scope.pageNum += direction;
		getAll();
	}
	//********************************
});

//*************************************************************************
//ADD controller
app.controller("addCtrl", function($scope, $location, $http){

	var baseUrl = "/api/zadaci"

	//********************************
	//novi objekat povezan sa formom
	$scope.new = {};
	$scope.new.naziv = "";
	$scope.new.zaduzeni = "";
	$scope.new.sati = "";
	$scope.new.opis = "";
	$scope.new.stanjeId="1";
	$scope.new.projekatId = "";
	
	//********************************

	$scope.save = function(){
		var promise = $http.post(baseUrl, $scope.new);
		promise.then(
			function success(res){
				$location.path("/main");
			},
			function error(res){
				alert("Ooops! An error has ocurred, plese try again!")
			}
		)
	}

	//u formi / ng-repeat
	var baseUrlProjekti = "/api/projekti/"
	$scope.projekti = [];

	var getProjekti = function(){
		var promise = $http.get(baseUrlProjekti);
		promise.then(
			function success(res){
				$scope.projekti = res.data;
			},
			function error(res){
				alert("Ooops! An error has ocurred, plese try again!")
			}
		)
	}
	getProjekti();
	//********************************

})

//*************************************************************************
//EDIT kontroler
app.controller("editCtrl", function($scope, $routeParams, $http, $location){
	
	var baseUrl = "/api/zadaci/";
	
	var id = $routeParams.aid;
	

	//dobavljanje starog objekta
	//**************************
	$scope.old = {};
	$scope.old.naziv = "";
	$scope.old.zaduzeni = "";
	$scope.old.sati = "";
	$scope.old.opis = "";
	$scope.old.stanjeId="";
	$scope.old.projekatId = "";
	
	var getOld = function(){
		$http.get(baseUrl + id).then(
			function success(res){
				$scope.old = res.data;
			},
			function error(res){
				alert("Ooops! An error has ocurred, plese try again!");
			}
		);
	}
	
	getOld();
	//*************************

	//cuvanje izmene
	$scope.save = function(){
		var promise = $http.put(baseUrl + id, $scope.old);
		promise.then(
			function success(res){
				$location.path("/main");
			},
			function error(res){
				alert("Ooops! An error has ocurred, plese try again!")
			}
		)
	}
	
	//dobavljanje za formu
	//********************
	var baseUrlProjekti = "/api/projekti/"
	$scope.projekti = [];


	var getProjekti = function(){
		$http.get(baseUrlProjekti).then(
			function success(res){
				$scope.projekti = res.data;
			},
			function error(res){
				alert("Something went wrong!");
			}
		);
	}
	getProjekti();
	//********************
});

//*************************************************************************
app.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl:'/app/html/home.html'
		})
		.when('/main', {
			templateUrl : '/app/html/glavni.html'
		})
		.when('/add', {
			templateUrl : '/app/html/add.html'
		})
		.when('/edit/:aid', {
			templateUrl : '/app/html/edit.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);