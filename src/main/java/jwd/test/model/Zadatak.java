package jwd.test.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="zadatak")
public class Zadatak {
	
	@Id
	@GeneratedValue
	@Column
	private Long id;
	
	
	@Column(nullable=false, unique=true)
	private String naziv;
	
	@Column(nullable=false)
	private String zaduzeni;
	
	@Column(nullable=false)
	private Integer sati;
	
	@Column
	private String opis;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Projekat projekat;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Stanje stanje;

	public Zadatak() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Zadatak(String naziv, String zaduzeni, Integer sati, String opis, Projekat projekat, Stanje stanje) {
		super();
		this.naziv = naziv;
		this.zaduzeni = zaduzeni;
		this.sati = sati;
		this.opis = opis;
		this.projekat = projekat;
		this.stanje = stanje;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getZaduzeni() {
		return zaduzeni;
	}

	public void setZaduzeni(String zaduzeni) {
		this.zaduzeni = zaduzeni;
	}

	public Integer getSati() {
		return sati;
	}

	public void setSati(Integer sati) {
		this.sati = sati;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Projekat getProjekat() {
		return projekat;
	}

	public void setProjekat(Projekat projekat) {
		this.projekat = projekat;
	}

	public Stanje getStanje() {
		return stanje;
	}

	public void setStanje(Stanje stanje) {
		this.stanje = stanje;
	}
	
	
	
	
}
