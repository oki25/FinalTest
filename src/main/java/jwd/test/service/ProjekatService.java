package jwd.test.service;


import org.springframework.data.domain.Page;

import jwd.test.model.Projekat;


public interface ProjekatService {
	Page<Projekat> findAll(int pageNum);
	Projekat findOne(Long id);
	void save(Projekat sajam);
	void remove(Long id);

}
