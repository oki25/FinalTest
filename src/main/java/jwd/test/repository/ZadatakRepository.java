package jwd.test.repository;


import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jwd.test.model.Zadatak;

@Repository
public interface ZadatakRepository 
	extends JpaRepository<Zadatak, Long> {

	Page<Zadatak> findByProjekatId(Long id, Pageable pageRequsest);

	@Query("SELECT z FROM Zadatak z WHERE " + 
			"(:naziv IS NULL OR z.naziv like :naziv)  AND" + 
			"(:zaduzeni IS NULL OR z.zaduzeni like :zaduzeni)  AND" + 
			"(:projekatId IS NULL OR z.projekat.id = :projekatId) ")
	Page<Zadatak> pretraga(
			@Param("naziv")String naziv,
			@Param("zaduzeni")String zaduzeni,
			@Param("projekatId")Long projekatId, 
			Pageable pageRequest);
}
