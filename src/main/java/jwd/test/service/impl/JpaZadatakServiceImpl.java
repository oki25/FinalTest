package jwd.test.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.test.model.Projekat;
import jwd.test.model.Stanje;
import jwd.test.model.Zadatak;
import jwd.test.repository.ZadatakRepository;
import jwd.test.service.ProjekatService;
import jwd.test.service.StanjeService;
import jwd.test.service.ZadatakService;

@Service
@Transactional
public class JpaZadatakServiceImpl implements ZadatakService {

	@Autowired
	private ZadatakRepository repository;

	@Autowired
	private StanjeService stanjeService;

	@Autowired
	private ProjekatService projekatService;

	@Override
	public Page<Zadatak> findAll(int pageNum) {
		return repository.findAll(new PageRequest(pageNum, 4));
	}

	@Override
	public Zadatak findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public void save(Zadatak value) {
		Stanje stanje = value.getStanje();
		Projekat projekat = value.getProjekat();

		stanje.getZadaci().add(value);
		projekat.getZadaci().add(value);

		stanjeService.save(stanje);
		projekatService.save(projekat);

		repository.save(value);
	}

	@Override
	public void remove(Long id) {
		repository.delete(id);
	}

	@Override
	public Page<Zadatak> findById(Long id, int pageNum) {
		// TODO Auto-generated method stub
		return repository.findByProjekatId(id, new PageRequest(pageNum, 4));
	}

	@Override
	public Page<Zadatak> pretraga(String naziv, String zaduzeni, Long projekatId, int pageNum) {

		if (naziv != null) {
			naziv = "%" + naziv + "%";
		}

		if (zaduzeni != null) {
			zaduzeni = "%" + zaduzeni + "%";
		}
		return repository.pretraga(naziv, zaduzeni, projekatId, new PageRequest(pageNum, 4));
	}

	@Override
	public void promeniStanje(Long id) {
		Zadatak zadatak = findOne(id);

		Long trenutno = zadatak.getStanje().getId();

		zadatak.setStanje(stanjeService.findOne(trenutno + 1l));
		save(zadatak);
	}

	@Override
	public Integer totalHours(List<Zadatak> list) {

		Integer ukupno = 0;

		for (Zadatak z : list) {
			ukupno += z.getSati();
		}

		return ukupno;
	}

}
