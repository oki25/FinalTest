package jwd.test.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="stanje")
public class Stanje {
	
	@Id
	@Column
	@GeneratedValue
	private Long id;
	
	@Column
	private String naziv;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy="stanje")
	private List<Zadatak> zadaci = new ArrayList<>();

	public Stanje(String naziv) {
		super();
		this.naziv = naziv;
	}

	public Stanje() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Stanje(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<Zadatak> getZadaci() {
		return zadaci;
	}

	public void setZadaci(List<Zadatak> zadaci) {
		this.zadaci = zadaci;
	}
	
	public void addZadatak(Zadatak zadatak) {
		if(!this.zadaci.contains(zadatak)) {
			this.zadaci.add(zadatak);
		}
	}
	

}
