package jwd.test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.test.model.Stanje;
import jwd.test.repository.StanjeRepository;
import jwd.test.service.StanjeService;

@Service
public class JpaStanjeServiceImpl implements StanjeService{

	@Autowired
	StanjeRepository repository;

	@Override
	public Page<Stanje> findAll(int pageNum) {
		return repository.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public Stanje findOne(Long id) {
		return repository.findOne(id);
	}

	@Override
	public void save(Stanje value) {
		repository.save(value);
		
	}
	
	
	
}
