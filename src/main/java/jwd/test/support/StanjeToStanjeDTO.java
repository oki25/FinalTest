package jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.test.model.Stanje;
import jwd.test.web.dto.StanjeDTO;

@Component
public class StanjeToStanjeDTO implements Converter<Stanje, StanjeDTO>{

	@Override
	public StanjeDTO convert(Stanje src) {
		StanjeDTO dto = new StanjeDTO();
		dto.setId(src.getId());
		dto.setNaziv(src.getNaziv());
		
		
		return dto;
	}
	
	public List<StanjeDTO> convert(List<Stanje> src){
		
		List<StanjeDTO> dtos = new ArrayList<>();
		
		
		for(Stanje x : src) {
			dtos.add(convert(x));
		}
		
		return dtos;
	}
	
	
}
