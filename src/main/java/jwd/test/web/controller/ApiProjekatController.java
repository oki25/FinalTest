package jwd.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.test.model.Projekat;

import jwd.test.service.ProjekatService;

import jwd.test.support.ProjekatToProjekatDTO;

import jwd.test.web.dto.ProjekatDTO;


@RestController
@RequestMapping(value="/api/projekti")
public class ApiProjekatController {
	@Autowired
	private ProjekatService service;
	
	@Autowired
	private ProjekatToProjekatDTO toDTO;
	
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ProjekatDTO>> getAll(@RequestParam(defaultValue="0")int pageNum){
		
		Page<Projekat> page = service.findAll(pageNum);
		
		List<Projekat> retVal = page.getContent();
		if(retVal == null || retVal.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<ProjekatDTO>>(toDTO.convert(retVal),
				HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<ProjekatDTO> getOne(
			@PathVariable Long id){
		
		Projekat retVal = service.findOne(id);
		
		if(retVal == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<ProjekatDTO>(
				toDTO.convert(retVal),
				HttpStatus.OK);
	}

}
