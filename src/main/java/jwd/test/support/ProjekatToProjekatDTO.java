package jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.test.model.Projekat;
import jwd.test.web.dto.ProjekatDTO;

@Component
public class ProjekatToProjekatDTO 
	implements Converter<Projekat, ProjekatDTO> {

	@Override
	public ProjekatDTO convert(Projekat src) {
		ProjekatDTO dto = new ProjekatDTO();
		dto.setId(src.getId());
		dto.setNaziv(src.getNaziv());
		dto.setRok(src.getRok());
		
		
		return dto;
	}

	public List<ProjekatDTO> convert(List<Projekat> src) {
		List<ProjekatDTO> dtos = new ArrayList<>();
		
		for(Projekat s: src){
			dtos.add(convert(s));
		}
		
		return dtos;
	}
}
