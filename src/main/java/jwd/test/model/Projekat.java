package jwd.test.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="projekat")
public class Projekat {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	
	
	@Column
	private String naziv;
	
	
	@Column
	private String rok;
	
	
	@OneToMany(mappedBy="projekat")
	private List<Zadatak> zadaci = new ArrayList<>();


	public Projekat() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Projekat(String naziv, String rok) {
		super();
		this.naziv = naziv;
		this.rok = rok;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getRok() {
		return rok;
	}


	public void setRok(String rok) {
		this.rok = rok;
	}


	public List<Zadatak> getZadaci() {
		return zadaci;
	}


	public void setZadaci(List<Zadatak> zadaci) {
		this.zadaci = zadaci;
	}
	
	public void addZadatak(Zadatak zadatak) {
		if(!this.zadaci.contains(zadatak)) {
			this.zadaci.add(zadatak);
		}
	}
	
}
