package jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


import jwd.test.model.Zadatak;
import jwd.test.service.ProjekatService;
import jwd.test.service.StanjeService;
import jwd.test.service.ZadatakService;
import jwd.test.web.dto.ZadatakDTO;

@Component
public class ZadatakDTOtoZadatak 
	implements Converter<ZadatakDTO, Zadatak>{
	
	@Autowired
	private ZadatakService zadatakService;
	@Autowired
	private ProjekatService projekatService;
	
	@Autowired
	private StanjeService stanjeService;
	
	@Override
	public Zadatak convert(ZadatakDTO dto) {
		Zadatak retVal;
		if(dto.getId()==null){
			retVal = new Zadatak();
		}else{
			retVal = zadatakService.findOne(dto.getId());
		}
		retVal.setId(dto.getId());
		retVal.setNaziv(dto.getNaziv());
		retVal.setOpis(dto.getOpis());
		retVal.setSati(dto.getSati());
		retVal.setZaduzeni(dto.getZaduzeni());
		retVal.setProjekat(projekatService.findOne(dto.getProjekatId()));
		retVal.setStanje(stanjeService.findOne(dto.getStanjeId()));
		
		
		return retVal;
	}

}
