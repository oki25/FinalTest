package jwd.test.service;

import org.springframework.data.domain.Page;

import jwd.test.model.Stanje;

public interface StanjeService {

	Page<Stanje> findAll(int pageNum);
	Stanje findOne(Long id);
	void save(Stanje value);
}
