package jwd.test.service.impl;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import jwd.test.model.Projekat;
import jwd.test.repository.ProjekatRepository;
import jwd.test.service.ProjekatService;

@Service
@Transactional
public class JpaProjekatServiceImpl implements ProjekatService {
	
	@Autowired
	private ProjekatRepository projekatRepository;

	@Override
	public Page<Projekat> findAll(int pageNum) {
		return projekatRepository.findAll(new PageRequest(pageNum, 5));
	}

	@Override
	public Projekat findOne(Long id) {
		return projekatRepository.findOne(id);
	}

	@Override
	public void save(Projekat value) {
		projekatRepository.save(value);
	}

	@Override
	public void remove(Long id) {
		projekatRepository.delete(id);
	}

	
	
}
